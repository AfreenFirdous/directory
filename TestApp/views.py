from django.shortcuts import render,redirect
from django.http import HttpResponse
from .models import Teacher
from .forms import TeacherForm
from django.views.generic import TemplateView, ListView


from django import forms
# Create your views here.
def Base(request):
    return render(request, "Base.html")

def Login(request):
    context = {
        'label': 'Login Failed'
    }
    if request.method == "POST":

        if Teacher.objects.filter(first_name=request.POST['username'], email=request.POST['Email']).exists():
            return redirect('/Create/')
    return render(request, 'Login.html', context=context)


def teacher_create_view(request):
    form = TeacherForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = TeacherForm()
        return redirect('/Detail/')

    context = {
        'form' : form

    }
    return render(request, "create.html", context)


class detaillist_view(ListView):
    context_object_name = 'item_list'
    queryset = Teacher.objects.all()
    template_name = 'detail.html'


def teacher_detail_view(request):
    Teacher.objects.order_by('last_name', 'subjects')

    obj = Teacher.objects.all()
    context = {
        'records' : obj,
    }
    return render(request, "detail.html", context)
