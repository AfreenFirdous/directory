from django.db import models
from django.core.validators import RegexValidator
# from django_mysql.models import ListCharField

# Create your models here.
class Teacher(models.Model):
    phone_regex = RegexValidator(regex=r'^\+971-?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")

    first_name = models.CharField(max_length=50)
    last_name  = models.CharField(max_length=50)
    # profile    = models.ImageField(upload_to='media')
    email      = models.EmailField(max_length=254, unique="true")
    phone      = models.CharField(max_length=17, blank=True)
    room       = models.CharField(max_length=10)
    subjects   = models.TextField(max_length=250) #Change it to list of subjects