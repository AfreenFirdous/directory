from django.urls import path
from . import views
from TestApp.views import teacher_detail_view, teacher_create_view

urlpatterns = [
    path('', views.Base, name='Base'),
    path('Login/', views.Login, name='Login'),
    # path('Detail/', ),
    path('Create/', teacher_create_view),
]