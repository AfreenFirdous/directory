# Generated by Django 3.0.7 on 2020-06-23 22:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TestApp', '0004_auto_20200624_0208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teacher',
            name='profile',
            field=models.ImageField(upload_to='media'),
        ),
    ]
